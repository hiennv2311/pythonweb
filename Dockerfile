FROM python:3.6
RUN pip install Flask
RUN mkdir /hello-python
WORKDIR /hello-python
COPY app.py /hello-python
CMD ["python", "app.py"]
