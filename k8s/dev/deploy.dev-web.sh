#
# #!/usr/bin/env bash
export GCR_PUSHER_KEY_PATH=/tmp/gcr-pusher.json
echo $GCR_PUSHER_KEY > $GCR_PUSHER_KEY_PATH
gcloud auth activate-service-account --key-file $GCR_PUSHER_KEY_PATH
gcloud container clusters get-credentials ${GKE_CLUSTER_NAME} --zone us-central1-a --project ${GCP_PROJECT_ID}

envsubst < k8s/dev/main.$SERVICE.yml > k8s-deployment.yml

kubectl apply -f k8s-deployment.yml

